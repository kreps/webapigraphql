﻿using GraphQL.AspNet.Attributes;
using GraphQL.AspNet.Controllers;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
        public class PersonsController : GraphController
        {
            [QueryRoot("person")]
            public Person RetrievePerson(int id)
            {
                // Normally you'd do a database lookup here
                return new Person()
                {
                    Id = id,
                    FirstName = "Luke",
                    LastName = "Skywalker",
                    ForceUser = true,
                    FavoriteSong = "Papa was a Rollin' Stone",
                };
            }
        }
}